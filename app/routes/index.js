const express = require('express');
const router = express.Router();
const handler = require('../handler')

router.get('/', (req,res)=>{
  res.json({
    success: true
  })
})
router.get('/user', handler.user.showUser)

module.exports = router;